If you forgot the password of your pop3 account but you remember the rule or
the pattern you used to create it, crackpop can help you to recover the
password.

Just build up a regular expression that you think matches the password and
crackpop will try to generate all the strings matchable by the regular
expression and try to access your pop3 account with them.

As an example let's try to hack Randall Munroe's account:

  $ ./crackpop.py \
      --host popmail.xkcd.example.net \
      --user randall \
      --pattern "[Cc]orrect [Hh]orse [Bb]attery [Ss]taple"


DEPENDENCIES

crackpop uses the 'exrex' python module, in case it is not packaged for your
system you can get it with 'pip'; example for Debian based systems:

  $ sudo aptitude install python-pip
  $ sudo pip install exrex


NOTE

The program always prints out the error status on failed authentication
attempts in order to let the user know what is going on.

This is because some POP3 servers may be more creative than others, and
sometimes it is possible to differentiate between authentication events only
by looking at the error message.

For instance popmail.libero.it is quite weird, this is what happens:

  - When the username is wrong the server replies with an expected:
      
      -ERR [AUTH] invalid user or password
      
  - When the username is right but the password is wrong the server replies
    with a different message:

      -ERR ERROR 119 invalid user or password err 30 

  - When the password is right the server replies with:

      -ERR [AUTH] POP3 access not allowed

    because it does not allow POP3 operations from users on networks different
    from its own (but it still allows _connections_ from other networks tho).

Yes, with pomail.libero.it it is possible to differentiate the "invalid user"
case from the "invalid password" one.

Have fun!
